<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = [
    	'nama', 'jk', 'tglLahir', 'alamat'
    ];

    public function penilaian(){
    	return $this->hasMany('App\Penilaian', 'karyawanId');
    }
}
