<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penilaian;
use App\DetailPenilaian;
use App\Kriteria;
use App\Karyawan;

use DB;

class PenilaianAHPController extends Controller
{
    public function index(){
        $data['no'] = '1';
        $data['penilaian'] = Penilaian::where('kategori', '1')->orderBy('hasil', 'desc')->get();

        return  view('page.penilaian.ahp.index', $data);
    }

    public function form(){
        $data['kriteria'] = Kriteria::where('kategori', '1')->get();
        $data['karyawan'] = Karyawan::orderBy('nama', 'asc')->get();

        return view('page.penilaian.ahp.form', $data);
    }

    public function view($id){
        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($id);
         return view('page.penilaian.ahp.view', $data);
    }

    public function store(Request $request){
        Penilaian::where('kategori', '1')->delete();

        $nilai = 0;
        $hasil = 0;
        $kriteria = Kriteria::where('kategori', '1')->get();
        $karyawan = Karyawan::orderBy('nama', 'asc')->get();

        foreach($karyawan as $kry){
            Penilaian::create([
                'karyawanId' => $kry->id,
                'kategori' => '1'
            ]);
            $penilaian = Penilaian::where('kategori', '1')->where('karyawanId', $kry->id)->first();

            // dd($kriteria);
            foreach($kriteria as $krt){
                $nilai = $krt->bobot * $request->nilai[$kry->id][$krt->id];
                $hasil += $nilai;
                DetailPenilaian::create([
                    'nilai' => round($nilai, 2),
                    'kriteriaId' => $krt->id,
                    'penilaianId' => $penilaian->id
                ]);
            }
            
            $penilaian->hasil = round($hasil, 2);
            $penilaian->save();
            $hasil = 0;
        }

        return redirect()->route('penilaian.ahp.index')->with('success', 'Berhasil melakukan penilaian.');
    }
}
