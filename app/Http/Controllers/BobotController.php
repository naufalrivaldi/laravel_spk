<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bobot;
use App\Kriteria;
use App\SubKriteria;

class BobotController extends Controller
{
    public function index(){
        return view('page.bobot.index');
    }

    public function ahp(){
        $data['no'] = '1';
        $data['kriteria'] = Kriteria::where('kategori', '1')->get();

        return view('page.bobot.ahp.index', $data);
    }

    public function saw(){
        $data['no'] = '1';
        $data['kriteria'] = Kriteria::where('kategori', '2')->get();

        return view('page.bobot.saw.index', $data);
    }

    public function ahpform(){
        $data['kriteria'] = Kriteria::where('kategori', '1')->get();
        
        return view('page.bobot.ahp.form', $data);
    }

    public function sawform(){
        $data['kriteria'] = Kriteria::where('kategori', '2')->get();
        
        return view('page.bobot.saw.form', $data);
    }

    public function subForm($kriteriaId){
        $data['kriteria'] = Kriteria::find($kriteriaId);
        $data['subkriteria'] = SubKriteria::where('kriteriaId', $kriteriaId)->get();
        
        return view('page.bobot.ahp.sub.form', $data);
    }

    public function ahpstore(Request $request){
        $idx = 0;
        $kriteria = Kriteria::where('kategori', '1')->get();
        $eigen = $this->eigenVector($request, $kriteria);

        foreach($kriteria as $row){
            $row->bobot = round($eigen[$idx], 2);
            $row->save();
            $idx++;
        }

        return redirect()->route('bobot.ahp')->with('success', 'Bobot berhasil diset.');
    }

    public function substore(Request $request){
        $idx = 0;
        $subkriteria = SubKriteria::where('kriteriaId', $request->kriteriaId)->get();
        $eigen = $this->eigenVector($request, $subkriteria);

        foreach($subkriteria as $row){
            $row->bobot = round($eigen[$idx], 2);
            $row->save();
            $idx++;
        }

        return redirect()->route('bobot.ahp')->with('success', 'Bobot berhasil diset.');
    }

    public function sawstore(Request $request){
        $kriteria = Kriteria::where('kategori', '2')->get();
        foreach($kriteria as $row){
            $row->bobot = round($request->bobot[$row->id], 2);
            $row->save();
        }

        return redirect()->route('bobot.saw')->with('success', 'Bobot berhasil diset.');
    }

    public function eigenVector($request, $kriteria){
        $jml = 0;
        $idx = 0;
        $data = [];
        $grandTotal = [];
        foreach($kriteria as $row){
            foreach($kriteria as $kr){
                $jml += $request->nilai[$kr->id][$row->id];
            }
            array_push($data, $jml);
            $jml = 0;
        }

        foreach($kriteria as $row){
            foreach($kriteria as $kr){
                $tmpNilai[$kr->id][$row->id] = $request->nilai[$kr->id][$row->id] / $data[$idx];
            }
            $idx++;
        }

        foreach($kriteria as $row){
            foreach($kriteria as $kr){
                $jml += $tmpNilai[$row->id][$kr->id];
            }
            array_push($grandTotal, round($jml / count($tmpNilai), 3));
            $jml = 0;
        }

        return $grandTotal;
    }
}
