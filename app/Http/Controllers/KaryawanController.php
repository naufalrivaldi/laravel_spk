<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KaryawanRequest;

use App\Karyawan;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['no'] = 1;
        $data['karyawan'] = Karyawan::all();
        return view('page.karyawan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['karyawan'] = (object)[
            'id' => '',
            'nama' => '',
            'jk' => '',
            'tglLahir' => '',
            'alamat' => ''
        ];
        return view('page.karyawan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KaryawanRequest $request)
    {
        Karyawan::create([
            'nama' => $request->nama,
            'jk' => $request->jk,
            'tglLahir' => $request->tglLahir,
            'alamat' => $request->alamat
        ]);

        return redirect()->route('karyawan.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['karyawan'] = Karyawan::find($id);

        return view('page.karyawan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KaryawanRequest $request, $id)
    {
        $data = Karyawan::find($id);
        $data->nama = $request->nama;
        $data->jk = $request->jk;
        $data->tglLahir = $request->tglLahir;
        $data->alamat = $request->alamat;
        $data->save();

        return redirect()->route('karyawan.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Karyawan::find($id);
        $data->delete();
        return redirect()->route('karyawan.index')->with('success', 'Delete data berhasil!');
    }
}
