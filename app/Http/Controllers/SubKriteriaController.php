<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubKriteriaRequest;

use App\Kriteria;
use App\SubKriteria;

class SubKriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['no'] = 1;
        $data['subkriteria'] = SubKriteria::all();
        return view('page.subkriteria.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kriteria'] = Kriteria::where('kategori', '1')->get();
        $data['subkriteria'] = (object)[
            'id' => '',
            'nama' => '',
            'kriteriaId' => ''
        ];
        return view('page.subkriteria.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubKriteriaRequest $request)
    {
        SubKriteria::create([
            'nama' => $request->nama,
            'kriteriaId' => $request->kriteriaId
        ]);

        return redirect()->route('subkriteria.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kriteria'] = Kriteria::all();
        $data['subkriteria'] = SubKriteria::find($id);

        return view('page.subkriteria.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubKriteriaRequest $request, $id)
    {
        $data = Subkriteria::find($id);
        $data->nama = $request->nama;

        if($data->kriteriaId  != $request->kriteriaId){
            $data->bobot = null;    
        }

        $data->kriteriaId = $request->kriteriaId;

        $data->save();

        return redirect()->route('subkriteria.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SubKriteria::find($id);
        $data->delete();
        return redirect()->route('subkriteria.index')->with('success', 'Delete data berhasil!');
    }
}
