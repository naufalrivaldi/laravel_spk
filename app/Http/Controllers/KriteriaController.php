<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KriteriaRequest;

use App\Kriteria;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['no'] = 1;
        $data['kriteria'] = Kriteria::all();
        return view('page.kriteria.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kriteria'] = (object)[
            'id' => '',
            'nama' => '',
            'kategori' => ''
        ];
        return view('page.kriteria.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KriteriaRequest $request)
    {
        Kriteria::create([
            'nama' => $request->nama,
            'kategori' => $request->kategori
        ]);

        return redirect()->route('kriteria.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kriteria'] = Kriteria::find($id);

        return view('page.kriteria.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KriteriaRequest $request, $id)
    {
        $data = kriteria::find($id);
        $data->nama = $request->nama;

        if($data->kategori != $request->kategori){
            $data->bobot = null;    
        }

        $data->kategori = $request->kategori;

        $data->save();

        return redirect()->route('kriteria.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kriteria::find($id);
        $data->delete();
        return redirect()->route('kriteria.index')->with('success', 'Delete data berhasil!');
    }
}
