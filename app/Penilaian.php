<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = 'penilaian';
    protected $fillable = [
    	'hasil', 'kategori', 'karyawanId'
    ];

    public function karyawan(){
    	return $this->belongsTo('App\Karyawan', 'karyawanId');
    }

    public function detailPenilaian(){
    	return $this->hasMany('App\detailPenilaian', 'penilaianId');
    }
}
