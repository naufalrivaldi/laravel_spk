<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenilaian extends Model
{
    protected $table = 'detail_penilaian';
    protected $fillable = [
    	'nilai', 'kriteriaId', 'penilaianId'
    ];
    public $timestamps = false;

    public function kriteria(){
    	return $this->belongsTo('App\Kriteria', 'kriteriaId');
    }

    public function penilaian(){
    	return $this->belongsTo('App\Penilaian', 'penilaianId');
    }
}
