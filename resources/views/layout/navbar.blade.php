<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">SPK</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="{{ route('dashboard.index') }}">Dashboard</a>

        <div class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="masterDropdown" role="button" data-toggle="dropdown">Master</a>

          <div class="dropdown-menu" aria-labelledby="masterDropdown">
            <a class="dropdown-item" href="{{ route('karyawan.index') }}">Karyawan</a>
            <a class="dropdown-item" href="{{ route('kriteria.index') }}">Kriteria</a>
            <a class="dropdown-item" href="{{ route('subkriteria.index') }}">Sub Kriteria</a>
            <a class="dropdown-item" href="{{ route('bobot.index') }}">Bobot</a>
          </div>
        </div>

        <a class="nav-item nav-link" href="{{ route('penilaian.index') }}">Penilaian</a>
      </div>
    </div>
  </div>
</nav>