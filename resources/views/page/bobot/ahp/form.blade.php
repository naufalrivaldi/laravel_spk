@extends('master')

@section('content')

<h5>Bobot Kriteria (AHP)</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('bobot.ahp') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ route('bobot.ahp') }}" method="POST">
          @csrf

          <table class="table table-bordered">
            <tr>
              <td>#</td>
              @foreach($kriteria as $row)
                <td>{{ $row->nama }}</td>
              @endforeach
            </tr>
            <tr>
              @foreach($kriteria as $row)
                <tr>
                  <td>{{ $row->nama }}</td>
                  @foreach($kriteria as $kr)
                    <td><input type="number" name="nilai[{{ $row->id }}][{{ $kr->id }}]" class="form-control" step="0.01" required></td>
                  @endforeach
                </tr>
              @endforeach
            </tr>
          </table>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        <hr>
        <h5>Detail Nilai</h5>
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Tingkat Kepentingan</th>
                <th>Definisi</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Sama Pentingnya</td>
                <td>Kedua elemen mempunyai pengaruh yang sama.</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Sedikit lebih penting</td>
                <td>Pengalaman dan penilaian sangat memihak satu elemen dibandingkan dengan pasangannya.</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Lebih Penting</td>
                <td>Satu elemen sangat disukai dan secara praktis dominasinya sangat nyata, dibandingkan dengan elemen pasangannya.</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Sangat Penting</td>
                <td>Satu elemen terbukti sangat disukai dan secara praktis dominasinya sangat nyata, dibandingkan dengan elemen pasangannya.</td>
              </tr>
              <tr>
                <td>9</td>
                <td>Mutlak lebih penting</td>
                <td>Satu elemen terbukti mutlak lebih disukai dibandingkan dengan pasangannya, pada keyakinan tertinggi.</td>
              </tr>
              <tr>
                <td>2,4,6,8</td>
                <td>Nilai Tengah</td>
                <td>Diberikan bila terdapat keraguan penilaian di antara dua tingkat kepentingan yang berdekatan.</td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>

@endsection