@extends('master')

@section('content')

<h5>Data Bobot AHP</h5>
<div class="row">
	<div class="col-md-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="kriteria-tab" data-toggle="tab" href="#kriteria" role="tab" aria-controls="kriteria" aria-selected="true">Kriteria</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="subkriteria-tab" data-toggle="tab" href="#subkriteria" role="tab" aria-controls="subkriteria" aria-selected="false">Sub Kriteria</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="kriteria" role="tabpanel" aria-labelledby="kriteria-tab">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('bobot.ahp.form') }}" class="btn btn-primary">Set Bobot</a>
          </div>
          <div class="card-body">
            <table class="table table-bordered dataTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Bobot</th>
                </tr>
              </thead>
              <tbody>
                @foreach($kriteria as $row)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->bobot }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="subkriteria" role="tabpanel" aria-labelledby="subkriteria-tab">
        @foreach($kriteria as $row)
        <div class="card mt-3">
          <div class="card-header">
            <h5>{{ $row->nama }}</h5>
          </div>
          <div class="card-header">
            <a href="{{ route('bobot.ahp.sub.form', ['kriteriaId' => $row->id]) }}" class="btn btn-primary">Set Bobot</a>
          </div>
          <div class="card-body">
            <table class="table table-bordered dataTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Bobot</th>
                </tr>
              </thead>
              <tbody>
                @php $no = 1; @endphp
                @foreach($row->subKriteria as $row)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->bobot }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div
</div>

@endsection