@extends('master')

@section('content')

<h5>Metode SPK</h5>
<div class="row">
	<div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>AHP</h3>
      </div>
      <div class="card-body">
        <a href="{{ route('bobot.ahp') }}" class="btn btn-primary btn-block">Bobot</a>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>SAW</h3>
      </div>
      <div class="card-body">
        <a href="{{ route('bobot.saw') }}" class="btn btn-primary btn-block">Bobot</a>
      </div>
    </div>
  </div>
</div>

@endsection