@extends('master')

@section('content')

<h5>Bobot Kriteria (SAW)</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('bobot.saw') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ route('bobot.saw') }}" method="POST">
          @csrf

          <table class="table table-bordered">
            <tr>
              <td>No</td>
              <td>Nama</td>
              <td>Bobot</td>
            </tr>
            <tr>
              @php $no = 1; @endphp
              @foreach($kriteria as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->nama }}</td>
                  <td><input type="number" name="bobot[{{ $row->id }}]" class="form-control" required></td>
                </tr>
              @endforeach
            </tr>
          </table>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection