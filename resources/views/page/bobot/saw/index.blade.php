@extends('master')

@section('content')

<h5>Data Bobot SAW</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('bobot.saw.form') }}" class="btn btn-primary">Set Bobot</a>
      </div>
      <div class="card-body">
        <table class="table table-bordered dataTable">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Bobot</th>
            </tr>
          </thead>
          <tbody>
            @foreach($kriteria as $row)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->bobot }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div
</div>

@endsection