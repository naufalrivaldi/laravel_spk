@extends('master')

@section('content')

<h5>Form Kriteria</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kriteria.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ (empty($kriteria->id))?route('kriteria.store'):route('kriteria.update', ['kriterium' => $kriteria->id]) }}" method="POST">
          @csrf
          @if(!empty($kriteria->id))
            @method('PUT')
          @endif

          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control {{ ($errors->has('nama'))?'is-invalid':'' }}" value="{{ $kriteria->nama }}">

            <!-- error -->
            @if($errors->has('nama'))
              <small class="text-danger">*{{ $errors->first('nama') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="kategori">Kategori</label>
            <select name="kategori" id="kategori" class="form-control {{ ($errors->has('kategori'))?'is-invalid':'' }}">
              <option value="">Pilih</option>
              <option value="1" {{ ($kriteria->kategori == '1')?'selected':'' }}>AHP</option>
              <option value="2" {{ ($kriteria->kategori == '2')?'selected':'' }}>SAW</option>
            </select>

            <!-- error -->
            @if($errors->has('kategori'))
              <small class="text-danger">*{{ $errors->first('kategori') }}</small>
            @endif
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection