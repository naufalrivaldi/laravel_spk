@extends('master')

@section('content')

<h5>Detail Penilaian</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('penilaian.ahp.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <h6>Nama</h6>
        <p>{{ $penilaian->karyawan->nama }}</p>
        <h6>Hasil</h6>
        <p>{{ $penilaian->hasil }}</p>
        <hr>
        <h6>Detail Penilaian</h6>
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Kriteria</th>
                <th>Nilai</th>
              </tr>
            </thead>
            <tbody>
              @foreach($penilaian->detailPenilaian as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->kriteria->nama }}</td>
                  <td>{{ $row->nilai }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection