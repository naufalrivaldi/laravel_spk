@extends('master')

@section('content')

<h5>Data Penilaian</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('penilaian.ahp.form') }}" class="btn btn-primary">Nilai Karyawan</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Hasil</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($penilaian as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->karyawan->nama }}</td>
                  <td>{{ $row->hasil }}</td>
                  <td>
                    <a href="{{ route('penilaian.ahp.view',['id' => $row->id]) }}" class="btn btn-info btn-sm">Lihat</a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection