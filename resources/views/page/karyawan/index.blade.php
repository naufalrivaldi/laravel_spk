@extends('master')

@section('content')

<h5>Data Karyawan</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('karyawan.create') }}" class="btn btn-primary">Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($karyawan as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->nama }}</td>
                  <td>{{ $row->jk }}</td>
                  <td>{{ $row->tglLahir }}</td>
                  <td>{{ $row->alamat }}</td>
                  <td>
                    <form action="{{ route('karyawan.destroy', ['karyawan' => $row->id]) }}" method="POST" onSubmit="return confirm('Anda yakin ingin menghapus data?')">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('karyawan.edit', ['karyawan' => $row->id]) }}" class="btn btn-success btn-sm">Edit</a>
                      <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection