@extends('master')

@section('content')

<h5>Form Karyawan</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('karyawan.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ (empty($karyawan->id))?route('karyawan.store'):route('karyawan.update', ['karyawan' => $karyawan->id]) }}" method="POST">
          @csrf
          @if(!empty($karyawan->id))
            @method('PUT')
          @endif

          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control {{ ($errors->has('nama'))?'is-invalid':'' }}" value="{{ $karyawan->nama }}">

            <!-- error -->
            @if($errors->has('nama'))
              <small class="text-danger">*{{ $errors->first('nama') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="jk">Jenis Kelamin</label>
            <select name="jk" id="jk" class="form-control {{ ($errors->has('jk'))?'is-invalid':'' }}">
              <option value="">Pilih</option>
              <option value="L" {{ ($karyawan->jk == 'L')?'selected':'' }}>Laki - laki</option>
              <option value="P" {{ ($karyawan->jk == 'P')?'selected':'' }}>Perempuan</option>
            </select>

            <!-- error -->
            @if($errors->has('jk'))
              <small class="text-danger">*{{ $errors->first('jk') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="tglLahir">Tanggal Lahir</label>
            <input type="date" name="tglLahir" id="tglLahir" class="form-control {{ ($errors->has('tglLahir'))?'is-invalid':'' }}" value="{{ $karyawan->tglLahir }}">

            <!-- error -->
            @if($errors->has('tglLahir'))
              <small class="text-danger">*{{ $errors->first('tglLahir') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" rows="5" class="form-control {{ ($errors->has('alamat'))?'is-invalid':'' }}">{{ $karyawan->alamat }}</textarea>

            <!-- error -->
            @if($errors->has('alamat'))
              <small class="text-danger">*{{ $errors->first('alamat') }}</small>
            @endif
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection